---
layout: page-avatar
title: Most software today is way to complicated
permalink: /about/
---

I believe interaction between computers and humans don’t have to be complicated and frustrating. Most software today does not account how users think and what they believe in. We can do better. We shouldn't strive to add feature after feature. Instead we must focus on the user and learn what she or he is expecting from our product and how we can fulfill their expectations.

I'd love to work on a project where I can solve real world problems using whatever technology is best suited for the job. I don't want to be limited to programming. I deliver the best results when I am able to work in design and development simultaneously while researching how the potential user is operating.

I started <del>iPhone OS</del>, nay iOS development back in 2007 when the first iPhone was released even before the App Store existed—a jailbreak was required. I’ve been working on mobile projects ever since. I still prefer Objective-C to Swift. It’s tooling has matured and most of the time is pretty reliable. Swift tends to evolve heavily in little time—which is okay since it is a new language—but isn’t an attribute you want in a long running client project. CoreData is my choice when persistence is needed.

<br/>
<br/>

If you feel friendly say [Hi!](mailto:florian@florianbuerger.com?subject=Hi!)
